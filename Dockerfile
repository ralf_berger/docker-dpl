FROM ruby:2.4-alpine

RUN apk add --no-cache git
RUN gem install --no-rdoc --no-ri \
    dpl json_pure jmespath aws-sdk-core aws-sdk-resources aws-sdk

ENV LANG en_US.utf8

WORKDIR /site
ENTRYPOINT ["dpl", "--skip_cleanup"]
CMD []
