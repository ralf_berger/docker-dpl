require 'dockerspec/serverspec'

describe 'Dockerfile' do
  describe docker_build('.') do

    it { should have_entrypoint ['dpl', '--skip_cleanup'] }
    it { should have_cmd        [] }
    it { should have_user       'nobody' }

  end
end
