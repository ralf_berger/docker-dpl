.PHONY: all build test push

all: build test push

build:
	docker build -t regreb/dpl .

test:
	bundle && bundle exec rspec

push:
	docker push regreb/dpl
