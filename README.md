# dpl Gem Deployment Container

## Usage

```sh
docker run regreb/dpl --provider=s3 [...]
```

See the [dpl docs](https://github.com/travis-ci/dpl) for details.

## Build, test, and push to Docker Hub

```sh
make
```
